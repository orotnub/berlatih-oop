<?php

require "animal.php";
require "Frog.php";
require "Ape.php";

echo "RELEASE 0<br>";
$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo var_dump($sheep->cold_blooded) . "<br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<br>RELEASE 1<br>";
$sungokong = new Ape("kera sakti");
$sungokong->yell();

echo "<br>";
$kodok = new Frog("kintel");
$kodok->jump();

?>